import React, { useState, useEffect } from 'react';
// Import global css
import '../styles/globals.css'
// Import CSS Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import NavBar from '../components/NavBar';
// Import Context Provider
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';
  
function MyApp({ Component, pageProps }) {
                // State hook for user state, define here for global scope 
                const [user, setUser] = useState({
                    // Initialized as an object with properties set as null
                    // Proper values will be obtained from localStorage AFTER component gets rendered due to Next.JS pre-rendering
                    id: null
                })

            // console.log(user);

            // Function for clearing local storage upon logout
            const unsetUser = () => {
                localStorage.clear();
                // Set the user global scope in the context provider to have its email set to null
                setUser({
                    id: null
                });
            }

            // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
            const fetchUserDetails = () => {
                const options = {
                    headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
                }

                fetch(`${ AppHelper.API_URL }/users/details`, options)
                .then(AppHelper.toJSON)
                .then((data) => {
                    console.log(data)
                    if (typeof data._id !== 'undefined') {
                        setUser({ id: data._id, categories:data.categories, records: data.records})
                    } else {
                        setUser({ id: null})   
                    }
                })
            };

           




          
            useEffect(() => {
                fetchUserDetails();
                  // Effect hook for testing the setUser() functionality
                console.log(user.id);
            }, [user.id])


        //             //state hook for user state, define here for global scope
        // const [user, setUser] = useState({
        //     id: null
        // })

        // //localStorage can only be accessed after this component has been rendered
        // useEffect(() => {
        //     setUser({
        //     id: localStorage.getItem('id')
        //     })
        // }, [])

        // //effect hook for testing the setUser() functionality
        // useEffect(() => {
        //     console.log(user.id);
        // }, [user.id])

        // //function for clearing local storage upon logout
        // const unsetUser = () => {
        //     localStorage.clear();

        //     //set the user global scope in the context provider  to have its
        //     //email and isAdmin set to null
        //     setUser({
        //     id: null,
        //     });
        // }


  return (
    <React.Fragment>
      {/* Wrap the component tree within the UserProvider context provider so that components will have access to the passed in values here */}
      <UserProvider value={{user, setUser, unsetUser, fetchUserDetails}}>
          <NavBar />
          <Container>
            <Component {...pageProps}/>
          </Container>
      </UserProvider>
    </React.Fragment>
  ) 
}

export default MyApp
