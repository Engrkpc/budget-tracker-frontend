import React from 'react';
import {Line} from 'react-chartjs-2';
import {useState, useEffect, useContext} from 'react';;
import UserContext from '../../UserContext';
import {Container, Form, NavItem} from 'react-bootstrap';
import moment from 'moment';
import AppHelper from '../../app-helper';

import { colorRandomizer, helloWorld } from '../../helpers';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Trend'}>
            <Row className="justify-content-center">	
                    < TrendPage />
            </Row>
        </View>
    )
}


const TrendPage = () =>{

    // const {user} = useContext(UserContext);

    const [records, setRecords] = useState([]);
   
    const [dateStart, setDateStart] = useState("");
    const [dateEnd, setDateEnd] = useState("");
    const [trendDates, setTrendDates] = useState("");
    const [balanceRecords, setBalanceRecords] = useState([]);


	// const [dates, setDates] = useState([]);
	// const [balanceTrend, setBalanceTrend] = useState([]);
	// const [newBalance, setNewBalance] = useState([]);
    
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }
        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            setRecords(data.records) 
        })
    }, [])

    console.log(records)
    useEffect(() =>{
        let datesArr =[];
        let amount = 0;
        let balance = [];
        records.forEach(record  => {
            if((moment(record.createdOn).format("YYYY-MM-DD") >= dateStart) && (moment(record.createdOn).format("YYYY-MM-DD") <= dateEnd)){
               
                if(datesArr.includes(moment(record.createdOn).format("YYYY-MM-DD")) !== true){
                //    console.log(moment(record.createdOn).format("YYYY-MM-DD"))
                    if(record.categoryType === "Income"){
                        amount=amount + record.amount
                        balance.push(amount)
                    }
                    if(record.categoryType === "Expense"){
                        amount=amount - record.amount
                        balance.push(amount)
                    }
                    datesArr.push(moment(record.createdOn).format("YYYY-MM-DD"))
                    console.log(datesArr)

                }else{
                   
                    if(record.categoryType === "Income"){
                        amount = balance[balance.length - 1] + record.amount
                        balance.pop();
                        balance.push(amount)
                    }
                    if(record.categoryType === "Expense"){
                        amount = balance[balance.length - 1] - record.amount
                        balance.pop();
                        balance.push(amount)
                    }
                    
                }
            }

            setTrendDates(datesArr);
            setBalanceRecords(balance)
        })
            
    }, [dateStart,dateEnd])

    // let arrLength = trendDates.length
    // console.log(arrLength)
    // setBalanceRecords(balanceRecords)
    

    console.log(trendDates)
    console.log(balanceRecords)
    const data = {
    labels: trendDates,
    datasets: [{
        label: 'Trends of Income and Expense',
        data: balanceRecords,
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1
    }]
    };

    return(
        <Container className="mt-5 pt-4 mb-5">
            <h3>Balance Trend</h3>
            <Form className="row">
                <Form.Group className="col-6">
                    <Form.Label>From</Form.Label>
                        <Form.Control
                            type="date"
                            value={dateStart}
                            onChange={e => setDateStart(e.target.value)}
                        />
                </Form.Group>

                <Form.Group className="col-6">
                    <Form.Label>To</Form.Label>
                        <Form.Control
                            type="date"
                            value={dateEnd}
                            onChange={e => setDateEnd(e.target.value)}
                        />
                </Form.Group>
                <hr/>
            </Form>
            <Line data={data}/>
        </Container>
        
     
    )
}