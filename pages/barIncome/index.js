import React, { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Monthly Expense'}>
            <Row className="justify-content-center">	
                    < BarIncome />
            </Row>
        </View>
    )
}


const BarIncome = () =>{

    const [records, setRecords] =  useState([]);
    const [monthlyIncome, setMonthlyIncome] = useState([]);

   
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }
        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            setRecords(data.records) 
        })
    }, [])

    const [ months, setMonths ] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
    
    
    useEffect(() =>{
        // console.log(records)
  
        setMonthlyIncome(months.map(month =>{
            let income = 0;
            records.forEach(record =>{
                if(record.categoryType === "Income"){
                    if(moment(record.createdOn).format("MMMM") === month){
                        income = income + record.amount
                    }
                }
            })
           return income
           
        }))
    },[records])
    
    console.log(monthlyIncome)
    const data = {
        labels: months,
        datasets: [{
            label: 'Monthly Income',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(255,99,132,0.2)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,0.4)',
            data: monthlyIncome
        }]
    }
    
    return(
        <Container>
            <div className="row">
                <div className="mt-3 col">
                    <h1 className="text-center mt-5">Monthly Income in PHP</h1>
                    <Bar data={data} />
                </div>
            </div>
        </Container>
        
    )
}