import React, { useState, useEffect, useContext } from 'react';
import UserContext, { UserProvider } from '../../UserContext'
import Router from "next/router";
import moment from 'moment';


import { Form } from 'react-bootstrap'
import FormControl from 'react-bootstrap/FormControl'
import {Button, Container, Card, InputGroup} from 'react-bootstrap'

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Records'}>
            <Row className="justify-content-center">	
                    < RecordPage />
            </Row>
        </View>
    )
}

const RecordPage = () => {
  
    const { user, fetchUserDetails } = useContext(UserContext);
    // console.log(user.records)
    const [searchResult, setSearchResult] = useState("");
    
    const [searchWord, setSearchWord] = useState("")

    const [searchType, setSearchType] = useState("All")

    const [bal, setBal] = useState(0);
    
    let countBalance = 0;

        //fetch first the user details
        useEffect(() =>{
            fetchUserDetails();
        }, [])


        useEffect(() =>{
            if(user.id !== null){
                for (let record of user.records){
                    if(record.categoryType === "Income"){
                        countBalance = countBalance + record.amount 
                        
                    }else if( record.categoryType === "Expense"){
                        countBalance = countBalance - record.amount
                    }
                }
                setBal(countBalance)
            }
        },[bal,searchType,searchWord,user.records])    

        // to rerender the fetch for data to reflect 
        
        useEffect(() =>{
            console.log(searchType)
            
            let expense=[];
            let income =[];
            let balance=[];

            if(user.id !== null){
                //map through all records
                const searchDiv = user.records.map(record =>{
                    console.log(searchWord)
                        if(searchType === "All"){
                            if(record.description.toLowerCase().includes(searchWord)){

                                if(record.categoryType === "Income"){
                                    balance.push(record.amount)
                                }else if(record.categoryType === "Expense"){
                                    balance.push(-Math.abs(record.amount))
                                }
                                return(
                                    <React.Fragment>
                                        <div className="row" key={record._id}>
                                            <div className="col-6" key={record.description}>
                                                <h5>{record.description}</h5>
                                                <h6>{record.categoryType}</h6>
                                                <p>{moment(record.createdOn).format("LL")}</p>
                                            </div>
                                            
                                                {record.categoryType === "Income" ?
                                                    <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                        <h6 className="text-primary">{record.amount}</h6>
                                                        <h5>{balance.reduce((accumulator, currentValue) =>{
                                                            return(accumulator + currentValue)
                                                            },0)}
                                                        </h5>
                                                    </div>
                                                :
                                                    <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                        <h6 className="text-danger">{record.amount}</h6>
                                                        <h5>{balance.reduce((accumulator, currentValue) =>{
                                                            return(accumulator + currentValue)
                                                            },0)}
                                                        </h5>
                                                    </div>

                                                }
                                
                                        </div> 
                                    </React.Fragment>
                                   
                                )
                            }
                        }else if(searchType === "Income"){
                            if(record.categoryType === "Income"){
                                if(record.description.toLowerCase().includes(searchWord)){

                                    if(record.categoryType === "Income"){
                                        income.push(record.amount)
                                   
                                    }
                                    return(
                                        <React.Fragment>
                                            <div className="row" key={record._id}>
                                                <div className="col-6" key={record.description}>
                                                    <h5>{record.description}</h5>
                                                    <h6>{record.categoryType}</h6>
                                                    <p>{moment(record.createdOn).format("LL")}</p>
                                                </div>
                                                <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                    <h6 className="text-success">{`+ ${record.amount}`}</h6>
                                                    <h5>{income.reduce((accumulator, currentValue) =>{
                                                        return(accumulator + currentValue)
                                                        },0)}
                                                    </h5>

                                                </div>
                                            </div> 
                                        </React.Fragment>
                                    )
                                }
                            }
                           
                        }else if( searchType === "Expense"){
                            if(record.categoryType === "Expense"){
                                if(record.description.toLowerCase().includes(searchWord)){
                                    
                                    if(record.categoryType === "Expense"){
                                        expense.push(record.amount)
                                    }
                                    return(
                                        <React.Fragment>
                                            <div className="row" key={record._id}>
                                                <div className="col-6" key={record.description}>
                                                    <h5>{record.description}</h5>
                                                    <h6>{record.categoryType}</h6>
                                                    <p>{moment(record.createdOn).format("LL")}</p>
                                                </div>
                                                <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                    <h6 className="text-danger">{`- ${record.amount}`}</h6>
                                                    <h5 className="text-danger">{expense.reduce((accumulator, currentValue) =>{
                                                        return(accumulator + currentValue)
                                                        },0)}
                                                    </h5>
                                                </div>
                                            </div> 
                                          
                                        </React.Fragment>
                                    )
                                }
                            }
                        }
                    
                })

                setSearchResult(searchDiv.reverse());
            }
           
        }, [searchWord, searchType,user.records])   

      
        console.log(bal)
        

     return(
         <Container className="mt-5 mb-5 pt-4">
            <h3>Records</h3>
                <InputGroup>
                    <InputGroup.Prepend>
                        <a className="btn btn-success" href="/records/new">Add</a>
                    </InputGroup.Prepend>

                    <FormControl placeholder="Search Record"
                        value={searchWord}
                        onChange={e => setSearchWord(e.target.value.toLowerCase())}
                    />
                    <Form.Control as="select" onChange={e => setSearchType(e.target.value)}>
                        <option value="All">All</option>
                        <option value="Income">Income</option>
                        <option value="Expense">Expense</option>
                    </Form.Control>
                </InputGroup>
                <Card className="mb-3">
                    <Card.Body>
                        <div>
                            <h3 className=" text-primary mb-3 text-right">
                                Current Balance: {`${bal}`} 
                            </h3>
                        </div>
                        <hr />
                            {searchResult}
                        
                    </Card.Body>
                </Card>
         </Container>
     )
 }