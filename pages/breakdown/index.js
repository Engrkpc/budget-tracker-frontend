import React from 'react';
import {Pie} from 'react-chartjs-2';
import {useState, useEffect, useContext} from 'react';;
import UserContext from '../../UserContext';
import {Container, Form, NavItem} from 'react-bootstrap';
import moment from 'moment';

import { colorRandomizer, helloWorld } from '../../helpers';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Breakdown'}>
            <Row className="justify-content-center">	
                    < PieChart />
            </Row>
        </View>
    )
}

const PieChart =() =>{


    const { user, fetchUserDetails } = useContext(UserContext);
    console.log(user.records)

    const [monthlyIncome, setMonthlyIncome] = useState([]);
    const [incomeName, setName] = useState([]);
    const [expenseName, setExpenseName] = useState([]);
    const [monthlyExpense, setMonthlyExpense] =  useState([]);
    const [dateStart, setDateStart] = useState("");
    const [dateEnd, setDateEnd] = useState("");
    const [bgColors, setBgColors] = useState([]);

    useEffect(() =>{
        fetchUserDetails();
    }, [])
        

    useEffect(() =>{
     
        if(dateStart !== "" && dateEnd !== ""){

            let income = [];
            let incomeLabel =[];
            let expense =[];
            let expenseLabel =[];

            user.records.forEach(record => {
                if((moment(record.createdOn).format("YYYY-MM-DD") >= dateStart) && (moment(record.createdOn).format("YYYY-MM-DD") <= dateEnd)){
                    if(record.categoryType === "Income"){
                        incomeLabel.push(record.categoryName)
                    }else if( record.categoryType === "Expense"){
                        expenseLabel.push(record.categoryName)
                    }
                }
            })
            setName(incomeLabel);
            setExpenseName(expenseLabel);
        

            user.records.forEach(record => {
                
                if((moment(record.createdOn).format("YYYY-MM-DD") >= dateStart) && (moment(record.createdOn).format("YYYY-MM-DD") <= dateEnd)){
                    if(record.categoryType === "Income"){
                        income.push(record.amount)
                    }else if(record.categoryType === "Expense"){
                        expense.push(record.amount)
                    }
                }
            })
            setMonthlyIncome(income)
            setMonthlyExpense(expense)

            setBgColors(user.records.map(() => `#${colorRandomizer()}`));

        }
    },[dateStart,dateEnd])


    console.log(incomeName)
    console.log(monthlyIncome)
   

    const dataIncome ={
        labels: incomeName,
        datasets: [{
            data: monthlyIncome,
            backgroundColor: bgColors
        }]
    }

    const dataExpense ={
        labels: expenseName,
        datasets: [{
            data: monthlyExpense,
            backgroundColor: bgColors
        }]
    }
    
    return(
        <Container className="mt-5 pt-4 mb-5">
            <h3>Income and Expense Category Breakdown</h3>
            <Form className="row">
                <Form.Group className="col-6">
                    <Form.Label>From</Form.Label>
                        <Form.Control
                            type="date"
                            value={dateStart}
                            onChange={e => setDateStart(e.target.value)}
                        />
                </Form.Group>

                <Form.Group className="col-6">
                    <Form.Label>To</Form.Label>
                        <Form.Control
                            type="date"
                            value={dateEnd}
                            onChange={e => setDateEnd(e.target.value)}
                        />
                </Form.Group>
                <hr/>
            </Form>
                <Container>
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            {/* check first if date selected are not empty before displaying the text */}
                            {(dateStart !== "" && dateEnd !== "") ?
                                 <h3 className="text-center">Income Breakdown</h3>
                                :
                                null
                            }
                            <Pie data={dataIncome} />
                        </div>
                         <div className="col-md-6 col-sm-12">
                         {/* check first if date selected are not empty before displaying the text */}
                            {(dateStart !== "" && dateEnd !== "") ?
                                 <h3 className="text-center">Expense Breakdown</h3>
                                :
                                null
                            }
                            <Pie data={dataExpense} />
                         </div>
                    </div>
                </Container>
        </Container>
    )
}

