import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';


import {Table,Button, Container} from 'react-bootstrap';
import Link from 'next/link';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Categories'}>
            <Row className="justify-content-center">	
                    < Categories />
            </Row>
        </View>
    )
}

const Categories = () => {

    const { user, fetchUserDetails } = useContext(UserContext);
    const [categoryList, setCategoryList] = useState([])

    useEffect(() =>{
        fetchUserDetails();
    }, [])
    
    useEffect(() =>{

        if(user.id !== null) {
            const categoryRows = user.categories.map(element =>{
                console.log(element.categoryName)
                console.log(element.categoryType)
                return(
                    <React.Fragment>
                        <tr key={element._id}>
                            <td><h6>{element.categoryName}</h6></td>
                            <td>{element.categoryType === "Income" ?
                                    <h6 className="text-primary">{element.categoryType}</h6>
                                    :
                                    <h6 className="text-danger">{element.categoryType}</h6>
                                }
                                
                            </td>
                        </tr>
                    </React.Fragment>
                   
                )
            });
            
            setCategoryList(categoryRows.reverse())
        }

        
    }, [categoryList, user.categories,user.records])

    return (
        <Container className="mt-5 pt-4 mb-5">
             <h3>Categories</h3>
             <Link href="/categories/new">
                <Button className="mt-1 mb-3">Add</Button>
             </Link>
             
             <Table className="table table-stripped table-bordered table-hover">
                 <thead>
                     <tr>
                         <th>Category</th>
                         <th>Type</th>
                     </tr>
                 </thead>
                 <tbody>
                     {categoryList}
                 </tbody>
             </Table>
        </Container>
       
    )
}