import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext'
import AppHelper from '../../app-helper'

import { Form } from 'react-bootstrap'
import {Button, Container, Card} from 'react-bootstrap'
import Router from 'next/router';

import Swal from 'sweetalert2';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Add Category'}>
            <Row className="justify-content-center">	
                    < NewCategory />
            </Row>
        </View>
    )
}


const NewCategory = () => {
    
    const { user } = useContext(UserContext);
 
    const [categoryName, setCategoryName] = useState('')
    const [categoryType, setCategoryType] = useState('')

    function addCategory(e){
        e.preventDefault()
        
        //ito para ma check ko kung naka login yung if may user.id pag true
        if(user.id){
          
            const options = {
                method: 'POST',
                headers: {
                     'Content-Type':'application/json',
                      Authorization: `Bearer ${localStorage.getItem('token') }` 
                },
                body: JSON.stringify({
                    categoryType: categoryType,
                    categoryName: categoryName
                })
            }

            fetch(`${ AppHelper.API_URL }/users/categories`, options)
            .then(AppHelper.toJSON)
            .then(data =>{
                console.log(data)
                Swal.fire('Category Added', `${categoryName} successfully added`, 'success')
                setCategoryName("");
                setCategoryType("");
                Router.push("/categories");
            })
        }else{
            console.log("user id is null")
        }

    }

    


    
    return(
       <Container className="mt-5 pt-4 mb-5 ">
           <div className="justify-content-center row">
               <div className="col-md-6 col">
                    <h3>New Category</h3>
                    <Card>
                        <Card.Header>Category Information</Card.Header>
                        <Card.Body>
                            <Form onSubmit={addCategory}>
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryName">Category Name:</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter category name"
                                        value={categoryName}
                                        onChange={e => setCategoryName(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Type:</Form.Label>
                                    <Form.Control as="select" placeholder="Enter category name" required
                                        onChange={e => setCategoryType(e.target.value)}
                                    >
                                            <option default>Select category</option>
                                            <option value="Income">Income</option>
                                            <option value="Expense">Expense</option>
                                    </Form.Control>
                                </Form.Group>
                                <Button className="btn btn-primary" type="submit">Submit</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>  
        </Container> 
    )
}