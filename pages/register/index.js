
import { useEffect, useState } from 'react';
import Router from 'next/router';

//bootstrap
import { Container } from 'react-bootstrap';
import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'Register'}>
            <Row className="justify-content-center">	
                    < RegisterPage />
            </Row>
        </View>
    )
}


const RegisterPage = () => {

    
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState(0);
    const [isActive, setIsActive] =useState(false)

    function register(event){
        event.preventDefault();

        // fetch in the database to check on existing email
        // fetch('http://localhost:4000/api/users/email-exists', {
        fetch('https://thawing-tor-77036.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				email:email
			})
		})

        .then(res => res.json())
        .then(data =>{
            console.log(data)

            //if no existing
            if(data === false){
                fetch('https://thawing-tor-77036.herokuapp.com/api/users',{
                    method: 'POST',
                    headers:{
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1 
                    })
                })
                .then(res => res.json())
                .then(data =>{
                    if(data){
                        setEmail('');
						setFirstName('');
						setLastName('');
						setMobileNo(0);
						setPassword1('');
						setPassword2('');

						Swal.fire('Register Successful','Redirecting to login', 'success')
                        //redirect to register
						Router.push('/');
                    }
                })

            }else{
                alert('email exists')
            }

        })



    }

    useEffect(() =>{
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    },[email,password1,password2])
   
    return(

        <Container >
            <h1 className="text-center">Register</h1>
            <Form onSubmit={register} className="col-lg-4 offset-lg-4 my-5">

                <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="First Name"
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Last Name"
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>


                <Form.Group>
                    <Form.Label>Mobile No.</Form.Label>
                    <Form.Control 
                        type="number"
                        placeholder="Mobile No."
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>


                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Confirm Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ?
                    <Button 
                        className="bg-primary"
                        type="submit"
                        id="submitBtn"
                    >
                        Register
                    </Button>
                    :
                    <Button 
                        className="bg-danger"
                        type="submit"
                        id="submitBtn"
                        disabled
                    >
                        Register
                    </Button>
                }
               
            </Form>
        </Container> 

    )
};