const colorRandomizer =() =>{
    return(
        //16777215 is the color white max value
        //
        Math.floor(Math.random()*16777215).toString(16)
    )
}

const helloWorld =() =>{
    console.log("Hello")
}

export { colorRandomizer, helloWorld };