import React from 'react';
import Head from 'next/head';
import { Container } from 'react-bootstrap';

const View = ({ title, children }) => {
    return (
        <React.Fragment>
            <Head>
                <title key="title-tag">{ title }</title>
                <meta key="title-meat" name="viewport" content="initila-scale=1.0, width=device-width" />
            </Head>
            <Container className="mt-5 pt-4 mb-5">
                { children }
            </Container>
        </React.Fragment>
    )
}

export default View;
//children parameter is a reserved keyword that contains the child/ sub component of another component  